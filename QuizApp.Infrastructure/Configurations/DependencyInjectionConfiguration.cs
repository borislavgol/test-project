﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using QuizApp.Domain.Entities;
using QuizApp.Domain.Interfaces;
using QuizApp.Persistance;
using QuizApp.Services.Quiz;
using QuizApp.Services.User;
using QuizApp.Services.Utils;

namespace QuizApp.Infrastructure.Configurations
{
    public static class DependencyInjectionConfiguration
    {
        public static void ConfigureServices(this IServiceCollection services, IConfiguration Configuration)
        {
            string connection = Configuration.GetConnectionString("DefaultConnection");

            services.AddDbContext<Context>(options =>
                options.UseSqlServer(connection));

            //services.AddTransient(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddTransient<IGenericRepository<UserEntity>, GenericRepository<UserEntity>>();
            services.AddTransient<IGenericRepository<TestAccessEntity>, GenericRepository<TestAccessEntity>>();

            services.AddScoped<IPasswordHash, PasswordHash>();

            services.AddScoped<IUserService, UserService>();

            services.AddScoped<IQuizService, QuizService>();
        }
    }
}
