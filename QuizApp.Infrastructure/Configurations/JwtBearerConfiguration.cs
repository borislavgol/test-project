﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using QuizApp.Services.Jwt;

namespace QuizApp.Infrastructure.Configurations
{
    public static class JwtBearerConfiguration
    {
        public static void ConfigureJwtBearer(this JwtBearerOptions options)
        {
            options.RequireHttpsMetadata = false;
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,

                ValidIssuer = AuthOptions.ISSUER,

                ValidateAudience = true,

                ValidAudience = AuthOptions.AUDIENCE,

                ValidateLifetime = true,

                IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),

                ValidateIssuerSigningKey = true,
            };
        }
    }
}
