﻿using AutoMapper;
using QuizApp.Domain.Entities;
using QuizApp.Domain.Models;

namespace QuizApp.Infrastructure.Maps
{
    public static class UserEntityMaps
    {
        public static IMapperConfigurationExpression AddUserEntityToModelMaps(this IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<UserEntity, UserModel>()
                .ForMember(m => m.Username, cfg => cfg.MapFrom(m => m.Username));

            return configuration;
        }
    }
}
