﻿using AutoMapper;
using QuizApp.Domain.Models;

namespace QuizApp.Infrastructure.Maps
{
    public static class TestModelsMaps
    {
        public static IMapperConfigurationExpression AddTestModelToTestInfoModelMaps(this IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<TestModel, TestInfoModel>()
                .ForMember(m => m.Id, cfg => cfg.MapFrom(m => m.Id))
                .ForMember(m => m.Name, cfg => cfg.MapFrom(m => m.Name))
                .ForMember(m => m.Description, cfg => cfg.MapFrom(m => m.Description))
                .ForMember(m => m.QuestionsCount, cfg => cfg.MapFrom(m => m.Questions.Length));

            return configuration;
        }
    }
}
