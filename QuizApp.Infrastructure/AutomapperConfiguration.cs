﻿using AutoMapper;
using QuizApp.Infrastructure.Maps;
using System;

namespace QuizApp.Infrastructure
{
    public class AutomapperConfiguration
    {
        public static MapperConfiguration ConfigureMapper(
            Action<IMapperConfigurationExpression> mapperConfiguration = null)
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddUserEntityToModelMaps();
                cfg.AddTestModelToTestInfoModelMaps();

                mapperConfiguration?.Invoke(cfg);

            });

            return configuration;
        }
    }
}
