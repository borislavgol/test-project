﻿using AutoMapper;
using Microsoft.IdentityModel.Tokens;
using QuizApp.Domain.Creditals;
using QuizApp.Domain.Entities;
using QuizApp.Domain.Interfaces;
using QuizApp.Domain.Models;
using QuizApp.Services.Jwt;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace QuizApp.Services.User
{
    public class UserService : IUserService
    {
        private readonly IPasswordHash _passwordHash;
        private readonly IMapper _mapper;
        private readonly IGenericRepository<UserEntity> _userEntityRepository;
        private readonly IGenericRepository<TestAccessEntity> _testAccessRepository;

        public UserService(
            IPasswordHash passwordHash,
            IMapper mapper,
            IGenericRepository<UserEntity> userEntityRepository,
            IGenericRepository<TestAccessEntity> testAccessRepository)
        {
            _passwordHash = passwordHash;
            _mapper = mapper;
            _userEntityRepository = userEntityRepository;
            _testAccessRepository = testAccessRepository;
        }

        public async Task<TokenModel> GetTokenFromCreditals(LoginPasswordModel loginPassword)
        {
            var user = _userEntityRepository.Find((each) =>
                    each.Username == loginPassword.Login &&
                    each.Password == _passwordHash.GetHashedPassword(loginPassword.Password))
                .First();

            var identity = GetIdentity(user.Username, user.Id);

            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var tokenModel = new TokenModel
            {
                Token = encodedJwt
            };

            return tokenModel;
        }

        public async Task<UserModel> GetUserInfo(int userId)
        {
            var user = _userEntityRepository.GetById(userId);

            if (user == null)
            {
                throw new KeyNotFoundException("User not found");

            }
            return _mapper.Map<UserModel>(user);
        }

        private ClaimsIdentity GetIdentity(string username, int userId, string role = "user")
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, username),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, role),
                    new Claim("userId", userId.ToString()),
                };

            ClaimsIdentity claimsIdentity =
            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}
