﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace QuizApp.Services.Jwt
{
    public class AuthOptions
    {
        public const string ISSUER = "Server";
        public const string AUDIENCE = "Client";
        public const int LIFETIME = 120;

        const string KEY = "t:)_n3rcF;^#]}!'";

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
