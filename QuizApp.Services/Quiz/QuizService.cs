﻿using AutoMapper;
using QuizApp.Domain.Entities;
using QuizApp.Domain.Interfaces;
using QuizApp.Domain.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApp.Services.Quiz
{
    public class QuizService : IQuizService
    {
        private readonly IGenericRepository<UserEntity> _userEntityRepository;
        private readonly IGenericRepository<TestAccessEntity> _testAccessRepository;
        private readonly IMapper _mapper;

        public QuizService(
            IMapper mapper,
            IGenericRepository<UserEntity> userEntityRepository,
            IGenericRepository<TestAccessEntity> testAccessRepository)
        {
            _mapper = mapper;
            _userEntityRepository = userEntityRepository;
            _testAccessRepository = testAccessRepository;
        }

        public async Task<TestInfoModel[]> GetAvailableTests(int userId)
        {
            var avialableTestsIdList = _testAccessRepository
                .Find((each) => each.UserId == userId)
                .Select((each) => each.TestId)
                .ToArray();

            var testsList = MockedData.GetTests().Where((test) => avialableTestsIdList.Contains(test.Id));

            var testsInfo = _mapper.Map<TestInfoModel[]>(testsList);

            return testsInfo;
        }

        public async Task<TestModel> GetTest(int testId, int userId)
        {
            _testAccessRepository.Find((each) => each.UserId == userId &&
                each.TestId == testId);

            return MockedData.GetTests().Where((test) => test.Id == testId).First();
        }

        public async Task<TestResultModel> SubmitTestAnswers(TestSubmitModel testSubmitModel, int userId)
        {
            _testAccessRepository.Find((each) => each.UserId == userId &&
                each.TestId == testSubmitModel.Id);

            var test = MockedData.GetTests().Where((test) => test.Id == testSubmitModel.Id).First();

            var result = new TestResultModel
            {
                Id = testSubmitModel.Id,
                Correct = GetNumberOfCorrectAnswers(testSubmitModel, test),
                Total = test.Questions.Length
            };

            return result;
        }

        private int GetNumberOfCorrectAnswers(TestSubmitModel testSubmitModel, TestModel testModel)
        {
            var results = new bool[testModel.Questions.Length];

            for (int i = 0; i < results.Length; i++)
            {
                results[i] = false;
            }

            foreach (var each in testSubmitModel.Answers)
            {
                var correctOptions = testModel.Questions[each.QuestionNumber].CorrectOptions;

                Array.Sort(correctOptions);
                Array.Sort(each.SelectedOptions);


                if (correctOptions.SequenceEqual(each.SelectedOptions))
                {
                    results[each.QuestionNumber] = true;
                }

            }

            return results.Where((each) => each).Count();
        }
    }
}
