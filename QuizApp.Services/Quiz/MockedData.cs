﻿using QuizApp.Domain.Models;

namespace QuizApp.Services.Quiz
{
    static class MockedData
    {
        public static TestModel[] GetTests()
        {
            var jsTest = new TestModel
            {
                Id = 1,
                Name = "JS Advanced",
                Description = "test your js skills",
                Questions = new QuestionModel[] {
                    new QuestionModel {
                        Type = "single",
                        Text = "Which of the following is true about variable naming conventions in JavaScript?",
                        Options = new string[] {
                            "JavaScript variable names must begin with a letter or the underscore character.",
                            "JavaScript variable names are case sensitive.",
                            "Both of the above.",
                            "None of the above."
                        },
                        CorrectOptions = new int[] { 2 }
                    },
                    new QuestionModel {
                        Type = "single",
                        Text = "Can you pass a anonymous function as an argument to another function?",
                        Options = new string[] {
                            "true",
                            "false"
                        },
                        CorrectOptions = new int[] { 0 }
                    },
                    new QuestionModel {
                        Type = "single",
                        Text = "Can you pass a anonymous function as an argument to another function?",
                        Options = new string[] {
                            "Yes",
                            "false"
                        },
                        CorrectOptions = new int[] { 0 }
                    },
                }
            };

            var cSharpTest = new TestModel
            {
                Id = 2,
                Name = "C# Advanced",
                Description = "test your C# skills",
                Questions = new QuestionModel[] {
                    new QuestionModel
                    {
                        Type = "single",
                        Text = "Which of the following gives the correct count of the constructors that a class can define?",
                        Options = new string[] {
                            "1",
                            "2",
                            "Any number",
                            "None of the above"
                        },
                        CorrectOptions = new int[] { 2 }
                    },
                    new QuestionModel
                    {
                        Type = "single",
                        Text = "What is the correct name of a method which has the same name as that of class and used to destroy objects?",
                        Options = new string[] {
                            "Constructor",
                            "Finalize()",
                            "Destructor",
                            "End"
                        },
                        CorrectOptions = new int[] { 2 }
                    },
                    new QuestionModel
                    {
                        Type = "multiple",
                        Text = "Choose all existing data types in C#",
                        Options = new string[] {
                            "int",
                            "string",
                            "number",
                            "bool",
                            "undefined"
                        },
                        CorrectOptions = new int[] { 0, 1, 3 }
                    },
                }
            };

            var pythonTest = new TestModel
            {
                Id = 3,
                Name = "Python Advanced",
                Description = "test your python skills",
                Questions = new QuestionModel[] {
                    new QuestionModel {
                        Type = "single",
                        Text = "Which type of Programming does Python support?",
                        Options = new string[] {
                            "object-oriented programming",
                            "structured programming",
                            "functional programming",
                            "all of the mentioned"
                        },
                        CorrectOptions = new int[] { 3 }
                    },
                    new QuestionModel {
                        Type = "single",
                        Text = "Which of the following is the correct extension of the Python file?",
                        Options = new string[] {
                            ".python",
                            ".pl",
                            ".py",
                            ".p"
                        },
                        CorrectOptions = new int[] { 2 }
                    },
                    new QuestionModel {
                        Type = "single",
                        Text = "Which of the following is used to define a block of code in Python language?",
                        Options = new string[] {
                            "Indentation",
                            "Key",
                            "Brackets",
                            "All of the mentioned"
                        },
                        CorrectOptions = new int[] { 0 }
                    },
                }
            };

            return new TestModel[] { jsTest, cSharpTest, pythonTest };
        }
    }
}