﻿namespace QuizApp.Domain.Models
{
    public class TestResultModel
    {
        public int Id { get; set; }
        public int Total { get; set; }
        public int Correct { get; set; }
    }
}
