﻿namespace QuizApp.Domain.Models
{
    public class TestInfoModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int QuestionsCount { get; set; } 
    }
}
