﻿namespace QuizApp.Domain.Models
{
    public class QuestionModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
        public string[] Options { get; set; }
        public int[] CorrectOptions { get; set; }
    }
}
