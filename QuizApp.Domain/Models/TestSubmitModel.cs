﻿namespace QuizApp.Domain.Models
{
    public class TestSubmitModel
    {
        public int Id { get; set; }

        public AnswerModel[] Answers { get; set; }
    }
}
