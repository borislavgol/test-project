﻿namespace QuizApp.Domain.Models
{
    public class AnswerModel
    {
        public int QuestionNumber { get; set; }
        public int[] SelectedOptions { get; set; }
    }
}
