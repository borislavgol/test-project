﻿namespace QuizApp.Domain.Entities
{
    public class TestAccessEntity : BaseEntity
    {
        public int UserId { get; set; }
        public int TestId { get; set; }
    }
}
