﻿using System.ComponentModel.DataAnnotations;

namespace QuizApp.Domain.Entities
{
    public abstract class BaseEntity
    {
        [Key]
        public int Id { get; set; }
    }
}
