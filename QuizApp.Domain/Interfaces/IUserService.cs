﻿using QuizApp.Domain.Creditals;
using QuizApp.Domain.Models;
using System.Threading.Tasks;

namespace QuizApp.Domain.Interfaces
{
    public interface IUserService
    {
        public Task<TokenModel> GetTokenFromCreditals(LoginPasswordModel loginPassword);

        public Task<UserModel> GetUserInfo(int userId);
    }
}
