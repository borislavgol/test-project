﻿namespace QuizApp.Domain.Interfaces
{
    public interface IPasswordHash
    {
        public string GetHashedPassword(string password);
    }
}
