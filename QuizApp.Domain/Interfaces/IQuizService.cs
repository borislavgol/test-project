﻿using QuizApp.Domain.Models;
using System.Threading.Tasks;

namespace QuizApp.Domain.Interfaces
{
    public interface IQuizService
    {
        public Task<TestInfoModel[]> GetAvailableTests(int userId);
        public Task<TestModel> GetTest(int testId, int userId);
        public Task<TestResultModel> SubmitTestAnswers(TestSubmitModel testSubmitModel, int userId);
    }
}
