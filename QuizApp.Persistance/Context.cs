﻿using Microsoft.EntityFrameworkCore;
using QuizApp.Domain.Entities;

namespace QuizApp.Persistance
{
    public class Context : DbContext
    {
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<TestAccessEntity> TestsAccess { get; set; }

        public Context(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserEntity>().HasData(new UserEntity { Id = 1, Username = "user1", Password = "pass1" });
            modelBuilder.Entity<UserEntity>().HasData(new UserEntity { Id = 2, Username = "user2", Password = "pass2" });
        }
    }
}
