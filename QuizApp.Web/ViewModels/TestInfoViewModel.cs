﻿namespace QuizApp.Web.ViewModels
{
    public class TestInfoViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int QuestionsCount { get; set; }
    }
}
