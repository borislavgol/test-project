﻿namespace QuizApp.Web.ViewModels
{
    public class QuestionViewModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
        public string[] Options { get; set; }
    }
}
