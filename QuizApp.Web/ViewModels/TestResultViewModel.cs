﻿namespace QuizApp.Web.ViewModels
{
    public class TestResultViewModel
    {
        public int Id { get; set; }
        public int Total { get; set; }
        public int Correct { get; set; }
    }
}
