export interface IQuestionModel {
  id: number;
  text: string;
  type: string;
  options: string[];
}