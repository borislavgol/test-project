export interface ITestResultModel {
  id: number;
  total: number;
  correct: number;
}