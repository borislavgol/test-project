import { IQuestionModel } from "./question";

export interface ITestModel {
  id: number;
  name: string;
  description: string;
  questions: IQuestionModel[];
}