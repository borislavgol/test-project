export interface ITestInfoModel {
  id: number;
  name: string;
  description: string;
  questionsCount: number;
}