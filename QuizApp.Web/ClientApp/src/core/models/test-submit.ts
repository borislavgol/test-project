export interface ITestSubmitModel{
  id: number;
  answers: IAnswer[];
}

export interface IAnswer{
  questionNumber: number;
  selectedOptions: number[];
}