export interface ILoginPasswordModel {
  login: string;
  password: string;
}