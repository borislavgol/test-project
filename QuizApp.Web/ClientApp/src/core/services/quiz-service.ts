import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of, tap } from "rxjs";
import { ITestModel } from "../models/test";
import { ITestInfoModel } from "../models/test-info";
import { ITestResultModel } from "../models/test-result";
import { ITestSubmitModel } from "../models/test-submit";
import { UserService } from "./user-service";

@Injectable({
  providedIn: 'root',
})

export class QuizService {
  private QUIZ_URL: string = 'api/Quiz';

  private _getQuiz = (id: number): string =>
    this.QUIZ_URL + "/" + id.toString();

  constructor(
    private _http: HttpClient,
    private _userService: UserService,
  ) { }

  public getTest(id: number): Observable<ITestModel> {
    return this._http.get<ITestModel>(this._getQuiz(id), { headers: { 'Authorization': 'Bearer ' + this._userService.getToken() } });
  }

  public submitTest(testSubmit: ITestSubmitModel): Observable<ITestResultModel> {
    return this._http.post<ITestResultModel>(this.QUIZ_URL, testSubmit, { headers: { 'Authorization': 'Bearer ' + this._userService.getToken() } });
  }

  public getAvailableTests(): Observable<ITestInfoModel[]> {
    return this._http.get<ITestInfoModel[]>(this.QUIZ_URL, { headers: { 'Authorization': 'Bearer ' + this._userService.getToken() } });
  }
}