import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of, tap } from "rxjs";
import { ILoginPasswordModel } from "../models/login-password";
import { ITokenModel } from "../models/token";
import { IUserModel } from "../models/user";

@Injectable({
  providedIn: 'root',
})

export class UserService {
  private USER_URL: string = 'api/User';

  public currentCachedUser: IUserModel;

  constructor(
    private _http: HttpClient,
  ) {
    this.currentCachedUser = null;
  }

  public getUserInfo(): Observable<IUserModel> {

    return this._fetchUserInfo(this.getToken());
  }

  public logIn(loginPassword: ILoginPasswordModel): Observable<any> {
    return this._http.post<ITokenModel>(this.USER_URL, loginPassword)
      .pipe(tap((res) => {
        this.setToken(res.token);

        return of();
      }));
  }

  public signOut(): void {
    this.currentCachedUser = null;
    this.clearToken();
  }

  public getToken(): string {
    return localStorage.getItem('token');
  }

  private setToken(token: string): void {
    return localStorage.setItem('token', token);
  }

  private clearToken(): void {
    localStorage.removeItem('token');
  }

  private _fetchUserInfo(token: string): Observable<IUserModel> {
    return this._http.get<IUserModel>(this.USER_URL, { headers: { 'Authorization': 'Bearer ' + token } })
      .pipe(tap((usr) => {
        this.currentCachedUser = usr;

        return of(usr);
      }));
  }
}