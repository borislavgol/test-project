import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { UserService } from 'src/core/services/user-service';
import { QuizService } from 'src/core/services/quiz-service';
import { AvailableTestsPageComponent } from './feature/pages/available-tests-page/available-tests-page.component';
import { LoginPageComponent } from './feature/pages/login-page/login-page.component';
import { TestPageComponent } from './feature/pages/test-page/test-page.component';
import { QuestionCoponent } from './feature/components/question/question.component';

@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    HomeComponent,
    LoginPageComponent,
    AvailableTestsPageComponent,
    TestPageComponent,
    QuestionCoponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'login', component: LoginPageComponent },
      { path: 'test', component: TestPageComponent },
      { path: 'available-tests', component: AvailableTestsPageComponent },
    ])
  ],
  providers: [
    UserService,
    QuizService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
