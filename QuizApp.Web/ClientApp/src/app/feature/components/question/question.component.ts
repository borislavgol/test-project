import { Component, EventEmitter, Input, Output } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'question',
  templateUrl: './question.component.html',
  styleUrls: ['question.component.scss']
})

export class QuestionCoponent {
  @Output() answer = new EventEmitter<number[]>();
  @Output() close = new EventEmitter();

  @Input()
  text: string;
  @Input()
  questionNumber: number;
  @Input()
  type: string;
  @Input()
  options: string[];
  @Input()
  totalQuestionsCount: number;

  form: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
  ) {
    this.form = _formBuilder.group({
      multipleAnswer: new FormArray([]),
      singleAnswer: ['', Validators.required]
    });


  }

  public onExitClicked(): void {
    this.close.emit();
  }

  public submit(): void {
    let result = null;

    if (this.type == 'single') {
      result = [this.form.controls.singleAnswer.value];

    } else if (this.type == 'multiple') {
      result = this.form.controls.multipleAnswer.value;
    }

    for(let i = 0; i < result.length; i++){
      result[i] = Number(result[i]);
    }

    this.answer.emit(result);

    this.form.patchValue({
      multipleAnswer: [],
      singleAnswer: '',
    })
  }

  onCheckChange(event) {
    const formArray: FormArray = this.form.get('multipleAnswer') as FormArray;

    if (event.target.checked) {

      formArray.push(new FormControl(event.target.value));
    }
    else {

      let i: number = 0;

      formArray.controls.forEach((ctrl: FormControl) => {
        if (ctrl.value == event.target.value) {

          formArray.removeAt(i);
          return;
        }

        i++;
      });
    }
  }
}
