import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ILoginPasswordModel } from 'src/core/models/login-password';
import { UserService } from 'src/core/services/user-service';

@Component({
  selector: 'login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['login-page.component.scss']
})

export class LoginPageComponent {
  public form: FormGroup;
  public processing: boolean;

  constructor(
    private _userService: UserService,
    private _router: Router,
  ) {
    this.processing = false;
    this.form = new FormGroup({
      "login": new FormControl('', Validators.required),
      "password": new FormControl('', Validators.required),
    });
  }

  public onBackClicked(): void {
    this._router.navigate(['']);
  }

  public onLoginClicked(): void {
    this.processing = true; 
    
    let loginPassword: ILoginPasswordModel = {
      ...this.form.value
    }

    this._userService.logIn(loginPassword).subscribe(
      {
        complete: () => {
          this._router.navigate(['']);
        },
        error: ()=>{
          this.processing = false;
        }
      }
    )
  }
}
