import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ITestModel } from "src/core/models/test";
import { ITestResultModel } from "src/core/models/test-result";
import { ITestSubmitModel } from "src/core/models/test-submit";
import { QuizService } from "src/core/services/quiz-service";

@Component({
  selector: 'test-page',
  templateUrl: './test-page.component.html',
  styleUrls: ['test-page.component.scss']
})

export class TestPageComponent implements OnInit {
  public started: boolean;
  public finished: boolean;
  public agreed : FormControl;

  private readonly _testId: number;
  private _test: ITestModel;
  private _currentQuestion: number;
  private _testSubmit: ITestSubmitModel;
  private _testResult: ITestResultModel;

  constructor(
    private _quizService: QuizService,
    private _route: ActivatedRoute,
    private _router: Router,
  ) {
    this._testId = Number(_route.snapshot.paramMap.get('id'));
    this.started = false;
    this.finished = false;
    this._currentQuestion = 0;

    this._testSubmit = {
      id: this._testId,
      answers: [],
    }

    this.agreed = new FormControl(false);
  }

  public ngOnInit(): void {
    this.initTest();
  }

  public get TestName(): string {
    return this._test.name;
  }

  public get TestDescription(): string {
    return this._test.description;
  }

  public get TestQuestionsCount(): number {
    return this._test.questions.length;
  }

  public get CurrentQuestionText(): string {
    return this._test.questions[this._currentQuestion].text;
  }

  public get CurrentQuestionOptions(): string[] {
    return this._test.questions[this._currentQuestion].options;
  }

  public get CurrentQuestionType(): string {
    return this._test.questions[this._currentQuestion].type;
  }

  public get CurrentQuestionNmber(): number {
    return this._currentQuestion;;
  }

  public get TotalCorrectAnswersFromResult(): number {
    return this._testResult.correct;
  }

  public get TotalAnswersCountFromResult(): number {
    return this._testResult.total;
  }

  public get CorrectAnswersPercentageFromResult(): number {
    return Math.round((100 / this._testResult.total) * this._testResult.correct);
  }

  public questionAnswered(answers: number[]): void {
    this._testSubmit.answers.push({ questionNumber: this._currentQuestion, selectedOptions: answers });

    if (this._currentQuestion + 1 == this.TestQuestionsCount) {
      this._quizService.submitTest(this._testSubmit).subscribe((result) => {
        this._testResult = result;
        this.finished = true;
      });
    } else {

      this._currentQuestion++;
    }

    console.log(this._testSubmit);
  }

  public closed(): void {
    this._router.navigate(['available-tests']);
  }

  public onStartClicked(): void {
    this.started = true;
  }

  private initTest() {
    this._quizService.getTest((this._testId)).subscribe((result) => {
      this._test = result;
    })
  }
}