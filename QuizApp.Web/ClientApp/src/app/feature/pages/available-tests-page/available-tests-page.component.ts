import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ITestInfoModel } from 'src/core/models/test-info';
import { QuizService } from 'src/core/services/quiz-service';
import { UserService } from 'src/core/services/user-service';

@Component({
  selector: 'available-tests-page',
  templateUrl: './available-tests-page.component.html',
  styleUrls: ['available-tests-page.component.scss']
})

export class AvailableTestsPageComponent {
  public tests: ITestInfoModel[];

  constructor(
    private _userService: UserService,
    private _quizService: QuizService,
    private _router: Router,
  ) {
    this.navToMainPageIfNoUser();
    this.updateTestsList();
  }

  get username(): string {
    return this._userService.currentCachedUser.username;
  }

  public signOutClicked(): void {
    this._userService.signOut();

    this._router.navigate(['']);
  }

  public goToTest(id: number): void {
    this._router.navigate(['test', { id: id }]);
  }

  private updateTestsList(): void {
    this._quizService.getAvailableTests()
      .subscribe((result) => {
        this.tests = result;
      });
  }

  private navToMainPageIfNoUser(): void {
    if (!this._userService.currentCachedUser)
      this._router.navigate(['']);
  }
}
