import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/core/services/user-service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public initialized: boolean;

  constructor(
    private _userService: UserService,
    private _router: Router,
  ) {
    this.initialized = false;
  }

  public ngOnInit(): void {
    this._initUser();
  }

  public onStartClicked(): void {
    this._userService.currentCachedUser ?
      this._router.navigate(['available-tests']) :
      this._router.navigate(['login']);
  }

  private _initUser(): void {
    this._userService.getUserInfo().subscribe(
      {
        complete: () => {
          this.initialized = true;
        },
        error: () => {
          this.initialized = true;
        }
      }
    )
  }
}
