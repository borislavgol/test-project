﻿using System.Security.Claims;

namespace QuizApp.Web.Configurations
{
    public static class JwtMethods
    {
        public static int GetUserIdFromClaimsPrincipal(ClaimsPrincipal claimsPrincipal)
        {
            return int.Parse(claimsPrincipal.FindFirst("userId").Value);
        }
    }
}
