﻿using AutoMapper;
using QuizApp.Domain.Creditals;
using QuizApp.Web.ViewModels;

namespace QuizApp.Web.Helpers.Maps
{
    public static class TokenViewModelMaps
    {
        public static IMapperConfigurationExpression AddTokenModelToViewModelMaps(this IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<TokenModel, TokenViewModel>()
                .ForMember(m => m.Token, cfg => cfg.MapFrom(m => m.Token));

            return configuration;
        }
    }
}
