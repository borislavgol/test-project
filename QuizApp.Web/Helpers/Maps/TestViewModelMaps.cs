﻿using AutoMapper;
using QuizApp.Domain.Models;
using QuizApp.Web.ViewModels;

namespace QuizApp.Web.Helpers.Maps
{
    public static class TestViewModelMaps
    {
        public static IMapperConfigurationExpression AddTestModelToViewModelMaps(this IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<TestModel, TestViewModel>()
                .ForMember(m => m.Id, cfg => cfg.MapFrom(m => m.Id))
                .ForMember(m => m.Name, cfg => cfg.MapFrom(m => m.Name))
                .ForMember(m => m.Description, cfg => cfg.MapFrom(m => m.Description))
                .ForMember(m => m.Questions, cfg => cfg.MapFrom(m => m.Questions));

            return configuration;
        }
    }
}
