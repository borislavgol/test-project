﻿using AutoMapper;
using QuizApp.Domain.Models;
using QuizApp.Web.ViewModels;

namespace QuizApp.Web.Helpers.Maps
{
    public static class QuestionViewModelMaps
    {
        public static IMapperConfigurationExpression AddQuestionModelToViewModelMaps(this IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<QuestionModel, QuestionViewModel>()
                .ForMember(m => m.Id, cfg => cfg.MapFrom(m => m.Id))
                .ForMember(m => m.Type, cfg => cfg.MapFrom(m => m.Type))
                .ForMember(m => m.Text, cfg => cfg.MapFrom(m => m.Text))
                .ForMember(m => m.Options, cfg => cfg.MapFrom(m => m.Options));

            return configuration;
        }
    }
}
