﻿using AutoMapper;
using QuizApp.Domain.Models;
using QuizApp.Web.ViewModels;

namespace QuizApp.Web.Helpers.Maps
{
    public static class UserViewModelMaps
    {
        public static IMapperConfigurationExpression AddUserModelToViewModelMaps(this IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<UserModel, UserViewModel>()
                .ForMember(m => m.Username, cfg => cfg.MapFrom(m => m.Username));

            return configuration;
        }
    }
}
