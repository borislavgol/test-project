﻿using AutoMapper;
using QuizApp.Domain.Models;
using QuizApp.Web.ViewModels;

namespace QuizApp.Web.Helpers.Maps
{
    public static class TestResultViewModelMaps
    {
        public static IMapperConfigurationExpression AddTestResultModelToViewModelMaps(this IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<TestResultModel, TestResultViewModel>()
                .ForMember(m => m.Id, cfg => cfg.MapFrom(m => m.Id))
                .ForMember(m => m.Total, cfg => cfg.MapFrom(m => m.Total))
                .ForMember(m => m.Correct, cfg => cfg.MapFrom(m => m.Correct));

            return configuration;
        }
    }
}
