﻿using AutoMapper;
using QuizApp.Domain.Models;
using QuizApp.Web.ViewModels;

namespace QuizApp.Web.Helpers.Maps
{
    public static class TestInfoViewModelMaps
    {
        public static IMapperConfigurationExpression AddTestInfoModelToViewModelMaps(this IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<TestInfoModel, TestInfoViewModel>()
                .ForMember(m => m.Id, cfg => cfg.MapFrom(m => m.Id))
                .ForMember(m => m.Name, cfg => cfg.MapFrom(m => m.Name))
                .ForMember(m => m.Description, cfg => cfg.MapFrom(m => m.Description))
                .ForMember(m => m.QuestionsCount, cfg => cfg.MapFrom(m => m.QuestionsCount));

            return configuration;
        }
    }
}
