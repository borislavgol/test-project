﻿using AutoMapper;
using QuizApp.Web.Helpers.Maps;
using System;

namespace QuizApp.Web.Helpers
{
    public static class AutoMapperViewConfig
    {
        public static Action<IMapperConfigurationExpression> AddWebMaps()
        {
            return cfg =>
            {
                cfg.AddTokenModelToViewModelMaps();
                cfg.AddUserModelToViewModelMaps();
                cfg.AddTestInfoModelToViewModelMaps();
                cfg.AddQuestionModelToViewModelMaps();
                cfg.AddTestModelToViewModelMaps();
                cfg.AddTestResultModelToViewModelMaps();
            };
        }
    }
}
