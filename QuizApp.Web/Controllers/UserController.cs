﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuizApp.Domain.Creditals;
using QuizApp.Domain.Interfaces;
using QuizApp.Domain.Models;
using QuizApp.Web.Configurations;
using QuizApp.Web.ViewModels;
using System.Threading.Tasks;

namespace QuizApp.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(
            IUserService userService,
            IMapper mapper)
        {
            _mapper = mapper;
            _userService = userService;
        }

        [HttpPost]
        public async Task<TokenViewModel> Login([FromBody] LoginPasswordModel loginPassword)
        {
            TokenModel tokenModel = await _userService.GetTokenFromCreditals(loginPassword);

            TokenViewModel tokenViewModel = _mapper.Map<TokenViewModel>(tokenModel);

            return tokenViewModel;
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet]
        public async Task<UserViewModel> GetUserInfo()
        {
            int userId = JwtMethods.GetUserIdFromClaimsPrincipal(User);

            UserModel userModel = await _userService.GetUserInfo(userId);

            UserViewModel userViewModel = _mapper.Map<UserViewModel>(userModel);

            return userViewModel;
        }

    }
}
