﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuizApp.Domain.Interfaces;
using QuizApp.Domain.Models;
using QuizApp.Web.Configurations;
using QuizApp.Web.ViewModels;
using System.Threading.Tasks;

namespace QuizApp.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class QuizController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IQuizService _quizService;

        public QuizController(
            IQuizService quizService,
            IMapper mapper)
        {
            _mapper = mapper;
            _quizService = quizService;
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet]
        public async Task<TestInfoViewModel[]> GetAvailableTests()
        {
            int userId = JwtMethods.GetUserIdFromClaimsPrincipal(User);

            TestInfoModel[] testInfoModel = await _quizService.GetAvailableTests(userId);

            TestInfoViewModel[] testInfoViewModel = _mapper.Map<TestInfoViewModel[]>(testInfoModel);

            return testInfoViewModel;
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet("{id}")]
        public async Task<TestViewModel> GetTest(int id)
        {
            int userId = JwtMethods.GetUserIdFromClaimsPrincipal(User);

            TestModel testModel = await _quizService.GetTest(id, userId);

            TestViewModel testViewModel = _mapper.Map<TestViewModel>(testModel);

            return testViewModel;
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPost]
        public async Task<TestResultViewModel> SubmitTestAnswers([FromBody] TestSubmitModel testSubmitModel)
        {
            int userId = JwtMethods.GetUserIdFromClaimsPrincipal(User);

            TestResultModel testResultModel = await _quizService.SubmitTestAnswers(testSubmitModel, userId);

            TestResultViewModel testResultViewModel = _mapper.Map<TestResultViewModel>(testResultModel);

            return testResultViewModel;
        }
    }
}
